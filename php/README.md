# PHP container layout



Container                           | Distribution name        | PHP Version                                                             
----------------------------------- | ------------------------ | ----------------
`registry.cmer.fr/php:ubuntu-12.04`        | precise                  | PHP 5.3
`registry.cmer.fr/php:ubuntu-14.04`        | trusty (LTS)             | PHP 5.5
`registry.cmer.fr/php:ubuntu-15.04`        | vivid                    | PHP 5.6
`registry.cmer.fr/php:ubuntu-15.10`        | wily                     | PHP 5.6
`registry.cmer.fr/php:ubuntu-16.04`        | xenial (LTS)             | PHP 7.0
`registry.cmer.fr/php:ubuntu-16.10`        | yakkety                  | PHP 7.0
`registry.cmer.fr/php:ubuntu-17.04`        | zesty                    | PHP 7.0
`registry.cmer.fr/php:ubuntu-17.10`        | artful                   | PHP 7.0
`registry.cmer.fr/php:ubuntu-18.04`        | bionic (LTS)             | PHP 7.2
`registry.cmer.fr/php:debian-7`            | wheezy                   | PHP 5.4
`registry.cmer.fr/php:debian-8`            | jessie                   | PHP 5.6
`registry.cmer.fr/php:debian-8-php7`       | jessie with dotdeb       | PHP 7.x (via dotdeb)
`registry.cmer.fr/php:debian-9`            | stretch                  | PHP 7.0
`registry.cmer.fr/php:centos-7`            |                          | PHP 5.4


## Filesystem layout

The whole docker directroy is deployed into `/opt/docker/`.

File                                                   | Description
------------------------------------------------------ | ------------------------------------------------------------------------------
`/opt/docker/etc/php/fpm/php-fpm.conf`                 | FPM daemon configuration
`/opt/docker/etc/php/fpm/pool.d/application.conf`      | FPM pool configuration


## Environment variables

Variable            | Description
------------------- | ------------------------------------------------------------------------------
`CLI_SCRIPT`        | Predefined CLI script for service
`APPLICATION_UID`   | PHP-FPM UID (Effective user ID)
`APPLICATION_GID`   | PHP-FPM GID (Effective group ID)

