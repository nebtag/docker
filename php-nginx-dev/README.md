# PHP with Nginx container layout



Container                               | Distribution name        | PHP Version
--------------------------------------- | ------------------------ | --------------
`registry.cmer.fr/php-nginx-dev:ubuntu-12.04`      | precise                  | PHP 5.3
`registry.cmer.fr/php-nginx-dev:ubuntu-14.04`      | trusty (LTS)             | PHP 5.5
`registry.cmer.fr/php-nginx-dev:ubuntu-15.04`      | vivid                    | PHP 5.6
`registry.cmer.fr/php-nginx-dev:ubuntu-15.10`      | wily                     | PHP 5.6
`registry.cmer.fr/php-nginx-dev:ubuntu-16.04`      | xenial (LTS)             | PHP 7.0
`registry.cmer.fr/php-nginx-dev:ubuntu-16.10`      | yakkety                  | PHP 7.0
`registry.cmer.fr/php-nginx-dev:ubuntu-17.04`      | zesty                    | PHP 7.0
`registry.cmer.fr/php-nginx-dev:ubuntu-17.10`      | artful                   | PHP 7.0
`registry.cmer.fr/php-nginx-dev:ubuntu-18.04`      | bionic (LTS)             | PHP 7.2
`registry.cmer.fr/php-nginx-dev:debian-7`          | wheezy                   | PHP 5.4
`registry.cmer.fr/php-nginx-dev:debian-8`          | jessie                   | PHP 5.6
`registry.cmer.fr/php-nginx-dev:debian-8-php7`     | jessie with dotdeb       | PHP 7.x (via dotdeb)
`registry.cmer.fr/php-nginx-dev:debian-9`          | stretch                  | PHP 7.0
`registry.cmer.fr/php-nginx-dev:centos-7`          |                          | PHP 5.4


## Environment variables

Variable              | Description
--------------------- |  ------------------------------------------------------------------------------
`CLI_SCRIPT`          | Predefined CLI script for service
`APPLICATION_UID`     | PHP-FPM UID (Effective user ID)
`APPLICATION_GID`     | PHP-FPM GID (Effective group ID)
`WEB_DOCUMENT_ROOT`   | Document root for Nginx
`WEB_DOCUMENT_INDEX`  | Document index (eg. `index.php`) for Nginx
`WEB_ALIAS_DOMAIN`    | Alias domains (eg. `*.vm`) for Nginx
`PHP_DEBUGGER`        | Either `xdebug`, `blackfire` or `none`. Default is `xdebug`.

## Filesystem layout

Directory                       | Description
------------------------------- | ------------------------------------------------------------------------------
`/opt/docker/etc/nginx`         | Nginx configuration
`/opt/docker/etc/nginx/ssl`     | Nginx ssl configuration with example server.crt, server.csr, server.key

File                                                | Description
--------------------------------------------------- | ------------------------------------------------------------------------------
`/opt/docker/etc/nginx/main.conf`                   | Main include file (will include `global.conf`, `php.conf` and `vhost.conf`) 
`/opt/docker/etc/nginx/global.conf`                 | Global nginx configuration options
`/opt/docker/etc/nginx/php.conf`                    | PHP configuration (connection to FPM)
`/opt/docker/etc/nginx/vhost.common.conf`           | Vhost common stuff (placeholder)
`/opt/docker/etc/nginx/vhost.conf`                  | Default vhost
`/opt/docker/etc/nginx/vhost.ssl.conf`              | Default ssl configuration for vhost
`/opt/docker/etc/php/fpm/php-fpm.conf`              | PHP FPM daemon configuration
`/opt/docker/etc/php/fpm/pool.d/application.conf`   | PHP FPM pool configuration

