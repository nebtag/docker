# Nginx webserver Docker container



Container                           | Distribution name                                                                 
----------------------------------- | -------------------
`registry.cmer.fr/nginx:ubuntu-12.04`      | precise
`registry.cmer.fr/nginx:ubuntu-14.04`      | trusty (LTS)
`registry.cmer.fr/nginx:ubuntu-15.04`      | vivid
`registry.cmer.fr/nginx:ubuntu-15.10`      | wily
`registry.cmer.fr/nginx:debian-7`          | wheezy
`registry.cmer.fr/nginx:debian-8`          | jessie
`registry.cmer.fr/nginx:centos-7`          |


## Environment variables

Variable               | Description
---------------------- | ------------------------------------------------------------------------------
`CLI_SCRIPT`           | Predefined CLI script for service
`APPLICATION_UID`      | PHP-FPM UID (Effective user ID)
`APPLICATION_GID`      | PHP-FPM GID (Effective group ID)
`WEB_DOCUMENT_ROOT`    | Document root for Nginx
`WEB_DOCUMENT_INDEX`   | Document index (eg. `index.php`) for Nginx
`WEB_ALIAS_DOMAIN`     | Alias domains (eg. `*.vm`) for Nginx

## Filesystem layout

Directory                       | Description
------------------------------- | ------------------------------------------------------------------------------
`/opt/docker/etc/nginx`         | Nginx configuration
`/opt/docker/etc/nginx/ssl`     | Nginx ssl configuration with example server.crt, server.csr, server.key

File                                          | Description
--------------------------------------------- | ------------------------------------------------------------------------------
`/opt/docker/etc/nginx/main.conf`             | Main include file (will include `global.conf`, `php.conf` and `vhost.conf`) 
`/opt/docker/etc/nginx/global.conf`           | Global nginx configuration options
`/opt/docker/etc/nginx/conf.d/*.conf`         | Global apache configuration directory (will be included)
`/opt/docker/etc/nginx/php.conf`              | PHP configuration (connection to FPM)
`/opt/docker/etc/httpd/vhost.common.d/*.conf` | Vhost common directory (will be included)
`/opt/docker/etc/nginx/vhost.conf`            | Default vhost
`/opt/docker/etc/nginx/vhost.ssl.conf`        | Default ssl configuration for vhost